#include "Simulator.h"
#include <iostream>
#include <math.h>

#define PI 3.14159265


Simulator::Simulator()
{
    camera_z = 0.8f;
    camera_y = 0.6f;
    waterLevel = -0.8f;
    y_pos = 1.5f;
    x_pos = 0;
    z_pos = -6.0f;
    freeze = true;
    gravity = 9.8/1000000;
    object = new Ball(0.3f);
    object->set_x(x_pos);
    object->set_y(y_pos);
    object->set_z(z_pos);
    aquarium.waterLevel = waterLevel;
}

Simulator::~Simulator()
{
    //dtor
}
void renderP(std::string str, GLfloat px, GLfloat py, GLfloat pz){
    int l = str.size(), i;
    float x = 0.0f;
    glPushMatrix();
        glRasterPos3f(px, py, pz-4.0f);
        for(i = 0; i < l; i++){
            glutBitmapCharacter(GLUT_BITMAP_8_BY_13, str[i]);
            x += 0.048f;
        }
    glPopMatrix();
}

void Simulator::startSimulator(){
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    gluLookAt(0, camera_y, camera_z,
              0, 0.5, camera_z-1,
              0, 0.5, 0);

    if (!freeze) {
        applyPhysics();
    }

     glMatrixMode(GL_MODELVIEW);
    //cout << object->speed << endl;
    glColor4f(1, 1, 1, 0.5);
    object->set_x(x_pos);
    object->set_y(y_pos);
    object->set_z(z_pos);

    object->render();

    aquarium.setWaterLevel(waterLevel);
    aquarium.renderAquarium();

    glColor4f(0, 0, 0, 1);
    renderP("DENSITY & BOUYANCY SIMULATOR", -0.45f, -0.5, camera_z);
    renderP("Controls: ", -0.45f, -0.6, camera_z);
    renderP("+", -0.4f, -0.7, camera_z);
    renderP("SPACE BAR to release ball", -0.4f, -0.8, camera_z);
    renderP("'s' to start again", -0.4f, -0.9, camera_z);



}
void Simulator::applyPhysics(){
    object->speed = object->speed - gravity;

    if (y_pos < waterLevel) {
        object->speed = object->speed + object->buoyant_force();
    }

    y_pos = y_pos + object->speed;

}
void Simulator::setCameraZ(GLfloat z){
    this->camera_z = z;
}
void Simulator::setCameraY(GLfloat y){
    this->camera_y = y;
}

