#include <windows.h>
#include <GL/glut.h>
#include <iostream>
#include <Ball.h>
#include <Aquarium.h>
#include <Simulator.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

int slices = 50;
int stacks = 50;
GLfloat y_pos = 1.5f;
GLfloat x_pos = 0;
GLfloat z_pos = -6.0f;
//GLfloat gravity = 0.000001f;
GLfloat speed = 0;
bool falling = false;
double a = 0;
GLfloat max_z = -5.0f;
GLfloat test = 1.0f;
GLfloat pin_y = -0.5f;
GLfloat pin_z = -9.0f;
GLfloat potential = 0.0006f;
GLfloat radius = 0.3f;
GLfloat side = 0.5f;
GLfloat camera_z = 0.1f;
GLfloat camera_y = 0.8f;
GLfloat eyes_y = 0.5f;
GLfloat lz = -0.1;
GLfloat zoom = 0.0f;
GLfloat weight = 0.01f;
bool object_ball;
bool object_lifebuoy;
bool object_box;
GLfloat mass2 = 0;
GLfloat still_y = 0;
bool simulationStart = false;
GLfloat waterLevel = -0.8f;
bool waterLevel2 = waterLevel;
GLfloat boundary = waterLevel+0.1;

float pi = 3.14159;
float velocity = 0;
float time = 0;
float gravity = 9.8/1000000;
float density_water = 0.9982; // g/mL

Simulator simulator;
bool start = false;


void initGL() {
    glClearColor(189/255.0f, 225/255.0f, 247/255.0f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    Ball ball(1);
    cout << ball.volume << endl;
    cout << ball.mass << endl;
    cout << ball.density << endl;
    cout << ball.submerge << endl;
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClear(GL_COLOR_BUFFER_BIT);
    Ball ball(radius);
    glColor4f(1, 1, 1, 0.3);
//    ball.setValues(radius, x_pos, y_pos, z_pos);
//    ball.renderBall();

    simulator.startSimulator();
    glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height) {
    if (height == 0) height = 1;
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}

void idle(void){
    glutPostRedisplay();
}

void resetDefaults(){

}

static void key(unsigned char key, int x, int y){
    if (key == 32) {
        simulator.freeze = false;
    }
    if (key == 's') {
        resetDefaults();
    }
    if (key == '-') {
        camera_z -= lz;
        simulator.setCameraZ(simulator.camera_z-lz);
        cout << camera_z << endl;
    }
    if (key == '=') {
        camera_z += lz;
        simulator.setCameraZ(simulator.camera_z+lz);
    }

    if (key == '1') {
        object_ball = true;
    }

    if (key == '2') {
        object_lifebuoy = true;
    }

    if (key == '3') {
        object_box = true;
    }


    glutPostRedisplay();
}

void specialKey(int key, int x, int y){
    glutSetKeyRepeat(1);

    switch(key) {
    case GLUT_KEY_UP:
        // case here
        break;
    case GLUT_KEY_DOWN:
        // case here
        break;
    }
}


void mouse(int button, int state, int x, int y) {
    float xClick = (float)x/GLUT_WINDOW_X;
    float yClick = (float)y/GLUT_WINDOW_Y;
}


void timer(int){
    glutPostRedisplay();
    glutTimerFunc(1000.0/60.0, timer, 0);
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    glutCreateWindow("Falling and Floating Simulation");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutKeyboardFunc(key);
    glutSpecialFunc(specialKey);
    glutMouseFunc(mouse);
    glutTimerFunc(1000.0/60.0, timer, 0);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    initGL();
    glutMainLoop();
    return 0;
}
