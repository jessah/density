#ifndef AQUARIUM_H
#define AQUARIUM_H
#include <GL/glut.h>
#include <iostream>

using namespace std;


class Aquarium
{
    public:
        Aquarium();
        virtual ~Aquarium();
        void renderAquarium();
        void setWaterLevel(GLfloat);
        GLfloat waterLevel;
    protected:
    private:
};

#endif // AQUARIUM_H
