#ifndef OBJECT_H
#define OBJECT_H
#include <GL/glut.h>
#include <string>

using namespace std;

class Object
{
    public:
        Object();
        virtual ~Object();
        GLfloat get_x();
        GLfloat get_y();
        GLfloat get_z();
        GLfloat x, y, z;
        void set_x(GLfloat);
        void set_y(GLfloat);
        void set_z(GLfloat);
        float volume;
        float mass;
        float density;
        float submerge;
        float speed;
        string material;
        virtual void render() = 0;
    protected:

    private:
};

#endif // OBJECT_H
