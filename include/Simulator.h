#ifndef SIMULATOR_H
#define SIMULATOR_H
#include <GL/glut.h>
#include <Aquarium.h>
#include <Object.h>
#include <Ball.h>
#include <iostream>

using namespace std;

class Simulator
{
    public:
        Simulator();
        virtual ~Simulator();
        void startSimulator();
        GLfloat camera_z;
        GLfloat camera_y;
        GLfloat waterLevel;
        void setCameraZ(GLfloat);
        void setCameraY(GLfloat);
        void setWaterLevel(GLfloat);
        GLfloat y_pos;
        GLfloat x_pos;
        GLfloat z_pos;
        bool freeze;
        void applyPhysics();
        float gravity;
        Object* object;
        Aquarium aquarium;

    protected:

    private:
};

#endif // SIMULATOR_H
