#ifndef BALL_H
#define BALL_H
#include <GL/glut.h>

#include <Object.h>

using namespace std;

class Ball : public Object
{
    public:
        Ball(GLfloat);
        virtual ~Ball();
        void set_radius(GLfloat);
        void renderBall();
        void setValues(GLfloat, GLfloat, GLfloat, GLfloat);
        GLfloat radius;
        GLfloat diameter;
        int slices, stacks;
        void render();
        float buoyancy();

    protected:

    private:
};

#endif // BALL_H
